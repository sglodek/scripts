echo -------------------------------------------------------
echo Emulate grep with sed. Print all lines starting with option.
echo -------------------------------------------------------
sed "/^\s*option/!d" sed_template

echo -------------------------------------------------------
echo Delete all blank lines in the file
echo -------------------------------------------------------
sed '/^$/d' sed_template

echo -------------------------------------------------------
echo 'Add the following line to the beginning of the file: # NASP19 sed DHCP Configuration'
echo -------------------------------------------------------
sed '1 i # NASP19 sed DHCP Configuration' sed_template

echo -------------------------------------------------------
echo 'Remove all comments at the end of line (hints : (1) what does ^ match? (2) what does [^] match? (3) what does [^^] match?'
echo -------------------------------------------------------
sed 's/\(^[^#].*\)\(#.*\)/\1/g' sed_template

echo -------------------------------------------------------
echo Change the router IP address to 10.10.0.1. Note that there is more than one empty space between "option" and "routers". Your expression should therefore match multiple empty spaces. To match any white space, use [[:space:]]. This will match tab, space, etc.
echo -------------------------------------------------------
sed 's/routers\s*[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*/routers 10.10.0.1/g' sed_template

echo -------------------------------------------------------
echo Report the range of IP addresses. The output should be in the form: IP range is : 10-100. I.e., do not report the rest of IP addresses.
echo -------------------------------------------------------
sed -e '/range/!d' -e 's/\([a-z]*\) \([0-9]*\.[0-9]*\.[0-9]*\.\)\([0-9]*\) \([0-9]*\.[0-9]*\.[0-9]*\.\)\([0-9]*\)\(.\)/IP \1 is: \3 - \5/' sed_template

echo -------------------------------------------------------
echo 'Prompt the user to change the address range (last octet in the /24 network). Your script shall accept a range, check the range for validity, and update the file. Do not assume that you know the network part of the range. Only the range itself should be updated by sed.'
echo -------------------------------------------------------
read -p "Please enter start of ip range (1-254): " \
  start_ip_range
read -p "Please enter end of ip range (1-254): " \
  end_ip_range
if [[ $start_ip_range -gt 0 && $start_ip_range -lt 255  && $end_ip_range -gt 0 && $end_ip_range -lt 255 && $end_ip_range -gt $start_ip_range ]];
 then
  sed -e '/range/!d' -e 's/[0-9]\{1,3\}\s/'$start_ip_range' /' -e 's/[0-9]\{1,3\};/'$end_ip_range' /' sed_template
else
  echo Invalid IP Range
fi
