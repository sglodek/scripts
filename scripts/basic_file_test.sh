set -o nounset
declare file_name=$1
declare exists
find $file_name > /dev/null
exists=$?
if [[ $exists == 0 ]] ; then
  echo The Filename: $file_name exists
  exit 0
else
  echo "The filename:" $file_name "can't be found"
  exit 1
fi
