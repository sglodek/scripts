#!/bin/bash

function send_process {
  process_id=$1
  signal=$2
  echo $process_id $signal
}

send_process $1 $2
