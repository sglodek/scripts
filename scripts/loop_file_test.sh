#!/bin/bash
set -o nounset
declare exists
declare file_name
function find_exists {

  file_name=$i
  find $file_name &> /dev/null
  exists=$?
  if [[ $exists == 0 ]] ; then
    echo The Filename: $file_name exists
  else
    echo "The filename:" $file_name "can't be found"
  fi
}

for i in $@; do
  find_exists $i
done
