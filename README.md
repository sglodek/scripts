~~~~
scripts
-------
All deliverables completed and follow suggested naming scheme

project
-------
All deliverables completed. Uses mostly custom naming scheme except for main.sh which is the only file you should need to run.

This script does the following:
1) Uses vboxmanage to create a VM
2) Mounts CentOS iso and installs using kickstart file
3) After install is finished, clones this machine into two additional VM's
4) Both machines will run mail and router startup scripts, and then reboot after a short period
5) Everything should be up and running at this point

Running the script again will leave the base vm, delete the clones, and recreate them with new scripts.


Running instructions:
1) Path to CentOS iso must be set in settings.cfg
2) NIC name as it shows up in Virtualbox must be set in settings.cfg
3) Wifi product id must be set settings.cfg. Run lsusb and then grab Y from the XXXX:YYYY string
3) Assuming you are running on vlan2016, all settings should work out of the box, otherwise you might need to change the IP's in settings.cfg
4) VM's are cloned from settings files in servers.conf, this is where you can turn services on and off.

Requirements:
Virtualbox
Tested on CentOS 7.5/MacOS 10.3
Should work on ubuntu systems using systemd, but was unable to finish checking

Notes:
-There is not a lot of error checking. If you put in wacky settings you will break it
-If for whatever reason the initial installation gets interrupted (Or iso/nic settings are incorrect) you will need to delete the base vm installation in virtualbox before running main.sh again

Unresolved issues:
-wifi spazzes out on bootup until hostapd starts running (this might be resolved in final build)
-initial vm_base boot requires a 60 second wait before installation begins (rebuilding the iso on the fly with a different boot file seemed time consuming)
-startup script run on first boot does not leave any logs
-first boot goes to login screen before script finishes running
-very delicate, easy to break if any settings are wrong
-subnets should be dynamically created
~~~~
