#!/usr/bin/env bash

#loading functions and settings
source ./functions.sh
source ./settings.cfg

#Creates base vm that others are cloned from
create_base_vm

#clones base_vm and creates servers based on conf files in servers.conf
create_servers
