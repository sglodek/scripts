if [ $server_type == 'router' ]; then
  server_ip=$router_ip
fi

if [ $server_type == 'mail' ]; then
  server_ip=$mail_ip
fi

source templates/ifcfg-enp0s3.template
source templates/ifcfg-enp0s8.template

cat > servers/$vm_name/scripts/startup.sh <<-EOF
#!/usr/bin/env bash
hostnamectl set-hostname $server_name.$domain
systemctl disable NetworkManager
systemctl stop NetworkManager
systemctl enable network
cp /mnt/cdrom/ifcfg-enp0s3 /etc/sysconfig/network-scripts/ifcfg-enp0s3

EOF

if [ $server_type == 'router' ]; then
  echo $server_type
  add_second_nic
  source templates/zebra.conf.template
  source templates/ospfd.conf.template
  echo router yes
  cat >> servers/$vm_name/scripts/startup.sh <<-EOF
  cp /mnt/cdrom/ifcfg-enp0s8 /etc/sysconfig/network-scripts/ifcfg-enp0s8
  systemctl enable iptables
  systemctl enable zebra
  systemctl enable ospfd
  cp /mnt/cdrom/zebra.conf /etc/quagga/zebra.conf
  cp /mnt/cdrom/ospfd.conf /etc/quagga/ospfd.conf
  echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
  
  iptables -t nat -A POSTROUTING -o enp0s8 -j MASQUERADE
  iptables -A FORWARD -p icmp -j ACCEPT
  iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
  iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
  iptables -A INPUT -p ospf -j ACCEPT
  iptables -A INPUT -p icmp -j ACCEPT
  iptables -A INPUT -p tcp --dport 22 -j ACCEPT
  iptables -A FORWARD -p tcp --dport 22 -j ACCEPT
  iptables -A INPUT -p tcp --dport 53 -j ACCEPT
  iptables -A INPUT -p udp --dport 53 -j ACCEPT
  iptables -A FORWARD -p tcp --dport 25 -j ACCEPT
  iptables -A FORWARD -p tcp --dport 143 -j ACCEPT
  iptables -A FORWARD -p tcp --dport 53 -j ACCEPT
  iptables -A FORWARD -p udp --dport 53 -j ACCEPT

  iptables -P INPUT DROP
  iptables -P FORWARD DROP

  iptables-save > /etc/sysconfig/iptables

EOF

  if [[ $dhcp == yes ]]; then
    echo dhcp yes
    source templates/dhcpd.conf.template
    sed -e '16 s/[0-9]\{1,3\}[[:space:]]/20 /' -e '16 s/[0-9]\{1,3\};/25;/' -e '22 s/[0-9]\{1,3\}[[:space:]]/20 /' -e '22 s/[0-9]\{1,3\};/25;/' servers/$vm_name/scripts/dhcpd.conf > servers/$vm_name/scripts/dhcpd2.conf
    cat >> servers/$vm_name/scripts/startup.sh <<-EOF
    cp /mnt/cdrom/dhcpd2.conf /etc/dhcp/dhcpd.conf
    systemctl enable dhcpd

EOF
  fi

  if [[ $unbound == yes ]]; then
    echo unbound yes
    source templates/unbound.conf.template
    cat >> servers/$vm_name/scripts/startup.sh <<-EOF
    cp /mnt/cdrom/unbound.conf /etc/unbound/unbound.conf
    systemctl enable unbound
    wget http://www.internic.net/domain/named.root -O /etc/unbound/root.hints

EOF
  fi

  if [[ $nsd == yes ]]; then
    echo nsd yes
    source templates/nsd.conf.template
    source templates/zone.template
    cat >> servers/$vm_name/scripts/startup.sh <<-EOF
    cp /mnt/cdrom/s08.as.learn.zone /etc/nsd/s08.as.learn.zone
    cp /mnt/cdrom/nsd.conf /etc/nsd/nsd.conf
    systemctl enable nsd

EOF
  fi

  if [[ $wifi == yes ]]; then
    echo wifi yes
    vboxmanage modifyvm $vm_name --usb on
    vboxmanage usbfilter add 1  --target $vm_name --name "wifi" --productid $wifi_product_id
    source templates/hostapd.conf.template
    source templates/ifcfg-wifi.template
    cat >> servers/$vm_name/scripts/startup.sh <<-EOF
    read current_wifi_name < <(ip a s | grep -o '[[:space:]]wlp[^:]*' | head -1)
    sed 's/wifi_name/'\$current_wifi_name'/' /mnt/cdrom/hostapd.conf > /etc/hostapd/hostapd.conf
    sed 's/wifi_name/'\$current_wifi_name'/' /mnt/cdrom/ifcfg-wifi > /etc/sysconfig/network-scripts/ifcfg-\$current_wifi_name
    systemctl enable hostapd

EOF
  fi

fi

if [ $server_type == 'mail' ]; then
  echo $server_type
  echo mail yes
  cat >> servers/$vm_name/scripts/startup.sh <<-EOF
  echo "GATEWAY=$router_ip" >> /etc/sysconfig/network-scripts/ifcfg-enp0s3
  echo "DNS1=$router_ip" >> /etc/sysconfig/network-scripts/ifcfg-enp0s3
  sed -e 's/#inet_interfaces/inet_interfaces/' -e '114,116 s/.*/ /g' -e '164 s/mydestination/#mydestination/' -e '165 s/#mydestination/mydestination/' -e '419 s/#home_mailbox/home_mailbox/' /etc/postfix/main.cf > /etc/postfix/main2.cf
  mv -f /etc/postfix/main2.cf /etc/postfix/main.cf

  sed '24 s/.*/protocols = imap lmtp/' /etc/dovecot/dovecot.conf > /etc/dovecot/dovecot2.conf
  mv -f /etc/dovecot/dovecot2.conf /etc/dovecot/dovecot.conf

  sed '30 s/.*/mail_location = maildir:~\/Maildir/' /etc/dovecot/conf.d/10-mail.conf > /etc/dovecot/conf.d/10-mail2.conf
  mv -f /etc/dovecot/conf.d/10-mail2.conf /etc/dovecot/conf.d/10-mail.conf

  sed '10 s/.*/disable_plaintext_auth = no/' /etc/dovecot/conf.d/10-auth.conf > /etc/dovecot/conf.d/10-auth2.conf
  mv -f /etc/dovecot/conf.d/10-auth2.conf /etc/dovecot/conf.d/10-auth.conf

  systemctl enable dovecot
EOF
fi



cat >> servers/$vm_name/scripts/startup.sh <<-EOF
systemctl disable firststartup
echo "done" >> /root/log.log
shutdown -r now
EOF
