systemctl disable NetworkManager
systemctl stop NetworkManager
systemctl enable network
systemctl start network
cp /run/install/repo/scripts/ifcfg-enp0s3 /mnt/sysimage/etc/sysconfig/network-scripts/ifcfg-enp0s3
systemctl start network
  echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
  iptables -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE
