source ./templates/.ks.cfg.template

if [[ $router == yes ]]; then
  echo router yes
  cat >> servers/$vm_name/scripts/ks.cfg <<-EOF
  yum install -y quagga
  systemctl enable zebra
  systemctl enable ospfd
  cp /mnt/cdrom/zebra.conf /etc/quagga/zebra.conf
  cp /mnt/cdrom/ospfd.conf /etc/quagga/ospfd.conf
  echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
  echo "iptables -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE" >> /etc/sysconfig/iptables
  iptables-save > /etc/sysconfig/iptables
EOF
fi

if [[ $dhcp == yes ]]; then
  echo dhcp yes
  cat >> servers/$vm_name/scripts/ks.cfg <<-EOF
  yum install -y dhcp
  cp /mnt/cdrom/dhcpd.conf /etc/dhcp/dhcpd.conf
  systemctl enable dhcpd

EOF
fi

if [[ $unbound == yes ]]; then
  echo unbound yes
  cat >> servers/$vm_name/scripts/ks.cfg <<-EOF
  yum install -y dhcp
  cp /mnt/cdrom/unbound.conf /etc/unbound/unbound.conf
  systemctl enable unbound
  wget http://www.internic.net/domain/named.root -O /etc/unbound/root.hints

EOF
fi

if [[ $nsd == yes ]]; then
  echo nsd yes
  cat >> servers/$vm_name/scripts/ks.cfg <<-EOF
  yum install -y dhcp
  cp /mnt/cdrom/nsd.conf /etc/nsd/nsd.conf
  systemctl enable nsd

EOF
fi

echo "%end" >> ./servers/$vm_name/scripts/ks.cfg
