echo "building unbound.conf"
cat > servers/$vm_name/scripts/unbound.conf <<-EOF

server:

	interface: $router_ip
	interface: 127.0.0.1
	interface: $wifi_ip

	# port to answer queries from
	port: 53

	# Enable IPv4, "yes" or "no".
	do-ip4: yes

	# Enable IPv6, "yes" or "no".
	do-ip6: no

	access-control: 0.0.0.0/0 allow
	access-control: ::0/0 refuse
	access-control: 127.0.0.0/8 allow
	access-control: 10.0.26.0/24 allow

	chroot: ""

	username: "unbound"

	directory: "/etc/unbound"

        root-hints: "/etc/unbound/root.hints"

	# the pid file. Can be an absolute path outside of chroot/work dir.
	pidfile: "/var/run/unbound/unbound.pid"

	# Allow the domain (and its subdomains) to contain private addresses.
	# local-data statements are allowed to contain private addresses too.
	private-domain: "s08.as.learn"

	# Declare the following local zones and allow restricted address by
	# disabling the default checks
	# Required for Reverse Lookups of private addresses used in NASP
        local-zone: "10.in-addr.arpa." nodefault
        local-zone: "16.172.in-addr.arpa." nodefault

	# if yes, perform prefetching of almost expired message cache entries.
	prefetch: yes

	# module configuration of the server. A string with identifiers
	# separated by spaces. "iterator" or "validator iterator"
	# we aren't performing and DNSsec validation so don't set it
	module-config: "iterator"

	#Include and other conf files
	include: /etc/unbound/local.d/*.conf

# Remote control config section.
remote-control:
	# Enable remote control with unbound-control(8) here.
	# set up the keys and certificates with unbound-control-setup.
	# Note: required for unbound-munin package
	control-enable: yes

	# unbound server key file.
	server-key-file: "/etc/unbound/unbound_server.key"

	# unbound server certificate file.
	server-cert-file: "/etc/unbound/unbound_server.pem"

	# unbound-control key file.
	control-key-file: "/etc/unbound/unbound_control.key"

	# unbound-control certificate file.
	control-cert-file: "/etc/unbound/unbound_control.pem"

#Include other configuration files
include: /etc/unbound/conf.d/*.conf

stub-zone:
        name: "$domain"
	stub-addr: $wan_ip

forward-zone:
 	name: "bcit.ca"
 	forward-addr: 142.232.221.253


EOF
