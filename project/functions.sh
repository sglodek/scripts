

function create_servers {
    for server_vars in ./servers.conf/*; do
      echo "Processing $server file.."

      #server variables
      source $server_vars
      delete_existing_vm
      #build folder structure for each server
      mkdir -p ./servers/$vm_name/iso/ ./servers/$vm_name/vm/ ./servers/$vm_name/scripts/

      clone_vm

      source script_builder.sh

      create_scripts_iso

      mount_scripts_iso

      vboxmanage startvm $vm_name
    done

}

function create_base_vm {
file $centos_iso
if [[ $? == 1 ]]; then
    echo "Cannot find CentOS ISO"
    exit
fi
  vboxmanage showvminfo vm_base &> /dev/null
  if [[ $? == 0 ]]; then
      echo "vm base already exists"
  else
    mkdir -p ./servers/vm_base/iso/
    mkdir -p ./servers/vm_base/scripts/
    mkdir -p ./servers/vm_base/vm/
    sed -e 's/wan_ip/'$wan_ip'/' -e 's/wan_gateway_ip/'$wan_gateway_ip'/'  ks.cfg > servers/vm_base/scripts/ks.cfg
    cp firststartup.service servers/vm_base/scripts
    mkisofs -l -quiet  -V OEMDRV -o ./servers/vm_base/iso/oemdrv.iso ./servers/vm_base/scripts/
    vboxmanage createvm --name vm_base --register --ostype RedHat_64
    vboxmanage modifyvm vm_base --nic1 bridged
    vboxmanage modifyvm vm_base --bridgeadapter1 $nic_name
    vboxmanage storagectl vm_base  --name controller --add sata --portcount 3
    vboxmanage modifyvm vm_base --memory 1024
    vboxmanage createmedium disk --filename ./servers/vm_base/vm/hdd.vdi --size 8000
    vboxmanage storageattach vm_base --port 1 --storagectl controller --type hdd --medium ./servers/vm_base/vm/hdd.vdi
    vboxmanage storageattach vm_base --port 2 --storagectl controller --type  dvddrive --medium ./$centos_iso
    vboxmanage storageattach vm_base --port 3 --storagectl controller --type  dvddrive --medium ./servers/vm_base/iso/oemdrv.iso
    vboxmanage startvm vm_base
    sleep 5
    until $(VBoxManage showvminfo --machinereadable vm_base | grep -q ^VMState=.poweroff.)
      do
        sleep 5
      done
  fi

}

function clone_vm {
  vboxmanage clonevm vm_base --name $vm_name --register
  vboxmanage modifyvm $vm_name --nic1 intnet
  vboxmanage modifyvm $vm_name --intnet1 as-net
  vboxmanage storageattach $vm_name --port 3 --storagectl controller --medium none

}

function mount_scripts_iso {
  vboxmanage storageattach $vm_name --port 2 --storagectl controller --type  dvddrive --medium ./servers/$vm_name/iso/oemdrv.iso
}

function add_second_nic {
    vboxmanage modifyvm $vm_name --nic2 bridged
    vboxmanage modifyvm $vm_name --bridgeadapter2 $nic_name
}

function delete_existing_vm {
  vboxmanage showvminfo $vm_name &> /dev/null
  if [[ $? == 0 ]]; then
      vboxmanage unregistervm $vm_name --delete
  fi
}

function create_scripts_iso {
  echo "creating iso"
  mkisofs -l -allow-multidot -relaxed-filenames -quiet -V OEMDRV -o ./servers/$vm_name/iso/oemdrv.iso ./servers/$vm_name/scripts/
}
